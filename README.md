# DEVELOPMENT CONTINUING IN WXMANDEL

# Environment setup

Clone or copy this repository

## Setup on a Mac
Install homebrew, glew and glm

```
$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
$ brew install glew
$ brew install glm
```

Download SDL2 from ```https://www.libsdl.org/release/SDL2-2.0.9.dmg``` and follow the installation instructions.

Download RapidJSON from ```https://github.com/Tencent/rapidjson/``` and save the contents of the include folder to ```packages/include```

Check the installation has worked:

```
$ make
$ ./mandel
```

## Setup on Windows
Install visual studio

**SDL2**
 - Download SDL2 from https://www.libsdl.org/release/SDL2-devel-2.0.9-VC.zip and unzip
 - Move the contents of the include folder to ```packages\include```
 - Move the lib files in ```lib\x86``` to ```packages\lib``` move the DLL to the root folder

**glew**
 - Download glew from https://sourceforge.net/projects/glew/files/glew/2.1.0/glew-2.1.0-win32.zip/download and unzip
 - Move the GL folder in the include folder to ```packages\include```
 - Move the ```lib\Release\Win32``` lib files to ```packages\lib```
 - Move the ```bin\Release\Win32``` dll to the root folder

 **glm**
 - Download glm from https://github.com/g-truc/glm/releases/tag/0.9.9.3 and unzip
 - Go into the first ```glm``` folder and check you can see the ```readme``` file. Take the the ```glm``` subfolder from here and move it to ```packages\include```

**RapidJSON**
 - Download RapidJSON from https://github.com/Tencent/rapidjson/
 - Move the contents of the include folder to ```packages\include```

**Check the installation has worked**

Open ```mandel.sln``` in visual studio, build the Debug/Win32 configuration and then run the mandel executable.

# Controls
- Scrolling zooms in or out of the fractal
- Dragging with mouse shifts the centre
- With the shift key down the mouse controls the colour map
  - up and down changes how sensitive the colours are to the iteration count
  - left and right offsets the starting colour
- With the shift key down scrolling increases or decreases the maximum number of iterations
- The p key toggle between double precision maths (zoom to around 10e-14) and quad precision (slower but good for zooms to around 10e-30)
- The q key stops the fractal
- The console window shows details of the iterations
- There are a bunch of parameters in src/settings.json which can be used to change the sensitivity of the controls.

# Problems
- The shader trys to check for large blocks of the same colour (e.g. inside the set) but can make mistakes (e.g. where the edge of the set is a sharp point).
- The shader trys to limit the amount of work in each frame but the mechanism is crude and doesn't automatically adjust to the performance of the machine. Reduce the framecap in the settings file if your machine struggles.
- The Windows build through Visual Studio is clunky. Would appreciate help
- The state model in OpenGL is very confusing and this is probably a very poor example. Would appreciate help.

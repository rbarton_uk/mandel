CXX = clang++
SDL = -framework SDL2
OGL = -framework OpenGL
# If your compiler is a bit older you may need to change -std=c++11 to -std=c++0x
CXXFLAGS = -g -Wall -c -std=c++11 -stdlib=libc++ -I/Library/Frameworks/SDL2.framework/Headers/ -I/usr/local/Cellar/glew/2.1.0/include  -I/usr/local/Cellar/glm/0.9.9.3/include -Ipackages/include
LDFLAGS = -F/Library/Frameworks -L/usr/local/Cellar/glew/2.1.0/lib -L/usr/local/Cellar/glm/0.9.9.3/lib $(SDL) $(OGL)

EXE = mandel

all: $(EXE)


$(EXE): main.o

	$(CXX) $(LDFLAGS) $< -o $@


main.o: ./src/main.cpp

	$(CXX) $(CXXFLAGS) $< -o $@

clean:

	rm *.o && rm $(EXE)
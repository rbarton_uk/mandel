#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <sstream>
using namespace std;

#include <stdlib.h>
#include <string.h>
#include <SDL.h>

#ifdef __APPLE__
#include <OpenGL/gl3.h>
#include <OpenGL/glu.h>
#include <glm/glm.hpp>
#else
#include <GL\glew.h>
#include <GL\gl.h>
#include <GL\glu.h>
#include <glm\glm.hpp>
#endif

using namespace glm;

#include <SDL_opengl.h>
#include "shader.hpp"

#include <rapidjson/document.h>
#include "rapidjson/filereadstream.h"
#include <cstdio>

using namespace rapidjson;

#ifdef __APPLE__
#define POINT_SCALE 1
#else
#define POINT_SCALE 1
#endif
int gMinRes = 128;
int gMaxThresh = 1000000;
int gMinThresh = 1000;
int gFrameCap = 25000 * 1024;

float gMoveFraction = 1.0/64/0;
float gZoomFraction = 0.05;
float gThreshFraction = 0.25;
float gHueFraction = 1.0/12.0;
float gHueStep = 0.5;

//Screen dimension constants
int gWinWidth = 1024;
int gWinHeight = 512;

void checkGLError(int l);

//Starts up SDL, creates window, and initializes OpenGL
bool init();

//Initializes rendering program and clear color
bool initGL();

bool handleWindow(SDL_WindowEvent w);

bool buildWinData();

//Input handler
bool handleKeys(unsigned char key, int x, int y);

//Per frame update
void update();

//Renders quad to the screen
void render();

//Frees media and shuts down SDL
void close();

//Shader loading utility programs
void printProgramLog(GLuint program);
void printShaderLog(GLuint shader);

//The window we'll be rendering to
SDL_Window *gGLWindow = nullptr;

//OpenGL context
SDL_GLContext gContext;

//Graphics program
GLuint gProgramID = 0;
GLint gParamsLocation = -1;
GLint gColMapLocation = -1;
GLint gThreshLocation = -1;
GLuint gVBO = 0;
GLuint gIBO = 0;
GLuint gVAB = 0;
GLuint gTFB = 0;
dvec4 gParamValues = dvec4(0.0, 0.0, 0.0, 0.0);
vec2 gColMapValues = vec2(0.0, 0.0);
GLint gThreshold = 0;
string gShaderPath = "";

int gNoVertices = 0;
int gNoCoords = 0;
GLfloat *gVBD = nullptr;
GLint *gVAD = nullptr;
GLint *gTFD = nullptr;
GLuint *gIBD = nullptr;

//start of high precision

struct int128
{
	int sign;
	uint val[4];
	int128(){
		sign = 0;
		for (int i=0; i<4; i++)
		{
			val[i]=0;
		}
	};
	int128(int s, uint* v){
		sign = s;
		for (int i=0; i<4; i++)
		{
			val[i]=v[i];
		}
	}
	int128(int s, uint v1, uint v2, uint v3, uint v4){
		sign = s;
		val[3]=v4;
		val[2]=v3;
		val[1]=v2;
		val[0]=v1;
	}
};

struct cplx128
{
	int128 r,i;
	cplx128(){
		r=int128();
		i=int128();
	}
	cplx128(int128 real, int128 image)
	{
		r=real;
		i=image;
	}
};

double from128(int128 a)
{
	const uint factor1 = 1 << 16;
	const double factor2 = double(factor1)*double(factor1);
	double r=0;

	if (a.sign == 0) return 0.0;
	for (int i=0; i < 4; i++)
	{
		r/=factor2;
		r+=a.val[i];
	}
	return r/factor1*a.sign;
}

int128 toInt128(double a)
{
	const uint factor1 = 1 << 16;
	const double factor2 = double(factor1)*double(factor1);

	int128 b = int128(0,0,0,0,0);

	if (a == 0) return b;
	double v = a;
	b.sign=1;
	if (v < 0) {
		b.sign = -1;
		v = -a;
	}
	v *= factor1;
	b.val[3] = uint(v);
	v -= b.val[3];
	v *= factor2;
	b.val[2] = uint(v);
	v -= b.val[2];
	v *= factor2;
	b.val[1] = uint(v);
	v -= b.val[1];
	v *= factor2;
	b.val[0] = uint(v);
	return b;
}

void split(uint a[], uint b[])
{
	int i=0;
	for (int j = 0; j < 4; j++)
	{
		b[i++]=a[j] & uint(0xFFFF);
		b[i++]=a[j] >> 16;
	}
}

int128 mult128 (int128 x, int128 y)
{
	int128 z = int128(0,0,0,0,0);
	uint xs[8], ys[8], rs[]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

	z.sign = x.sign * y.sign;
	if (z.sign == 0) return z;
	split(x.val, xs);
	split(y.val, ys);
	for (int i=0; i<8; i++)
	{
		for (int j=0; j<8; j++)
		{
			rs[i+j]+=xs[i]*ys[j];
		}
	}
	for (int i=0; i<15;i++)
	{
		rs[i+1]+=rs[i] >> 16;
		rs[i] = rs[i] & uint(0xFFFF);
	}
	z.val[3] = (rs[14] << 16) + rs[13];
	z.val[2] = (rs[12] << 16) + rs[11];
	z.val[1] = (rs[10] << 16) + rs[9];
	z.val[0] = (rs[8] << 16) + rs[7];
	return z;
}

int diffval (uint x[], uint y[], uint d[])
{
	int carry = 0;
	for (int i = 0; i < 4; i++)
	{
		d[i]=x[i];
		if (carry > 0) {
			d[i]--;
			carry = 0;
		}
		if (d[i] < y[i]) {
			carry = 1;
			d[i]+= ~y[i];
		} else {
			d[i]-=y[i];
		}
	}
	return -carry;
}

void sumval (uint x[], uint y[], uint s[])
{
	uint xs[8], ys[8], rs[]={0,0,0,0,0,0,0,0,0};

	split(x, xs);
	split(y, ys);
	for (int i=0; i < 8; i++)
	{
		rs[i] = xs[i]+ys[i];
	}
	for (int i=0; i<8;i++)
	{
		rs[i+1]+=rs[i] >> 16;
		rs[i] = rs[i] & uint(0xFFFF);
	}
	s[3] = (rs[7] << 16) + rs[6];
	s[2] = (rs[5] << 16) + rs[4];
	s[1] = (rs[3] << 16) + rs[2];
	s[0] = (rs[1] << 16) + rs[0];
}

int valgt (uint x[], uint y[])
{
	for (int i=3; i >= 0; i--)
	{
		if (x[i] > y[i]) return 1;
		if (x[i] < y[i]) return -1;
	}
	return 0;

}

int signxmy128 (int128 x, int128 y)
{
	if (x.sign > y.sign) return 1;
	if (y.sign < y.sign) return -1;
	for (int i=3; i >= 0; i--)
	{
		if (x.val[i] > y.val[i]) return x.sign;
		if (x.val[i] < y.val[i]) return -x.sign;
	}
	return 0;
}

int128 add128 (int128 x, int128 y)
{
	int128 z = int128(0,0,0,0,0);

	if (x.sign == 0) return y;
	if (y.sign == 0) return x;
	if (x.sign < 0 && y.sign > 0) {
		int valsign = valgt(x.val, y.val);
		if (valsign == 0) return z;
		if (valsign == 1){
			z.sign = -1;
			diffval(x.val,y.val,z.val);
			return z;
		} else {
			z.sign = 1;
			diffval(y.val, x.val, z.val);
			return z;
		}
	}
	if (y.sign < 0 && x.sign > 0) {
		int valsign = valgt(x.val, y.val);
		if (valsign == 0) return z;
		if (valsign == 1){
			z.sign = 1;
			diffval(x.val,y.val,z.val);
			return z;
		} else {
			z.sign = -1;
			diffval(y.val, x.val, z.val);
			return z;
		}
	}
	z.sign=x.sign;
	sumval(x.val,y.val,z.val);
	return z;
}

int128 sub128 (int128 x, int128 y)
{
	int128 z = int128(0,0,0,0,0);

	if (x.sign == 0) {
		z = int128(-y.sign,y.val);
		return z;
	}
	if (y.sign == 0) return x;
	if (y.sign < 0) {
		z = int128(1,y.val);
		return add128(x, z);
	}
	if (x.sign < 0) {
		z =int128(1,x.val);
		z = add128(z, y);
		z.sign = -1;
		return z;
	}
	int valsign = valgt(x.val, y.val);
	if (valsign == 0) return z;
	if (valsign == 1)
	{
		z.sign = 1;
		diffval(x.val,y.val,z.val);
		return z;
	} else {
		z.sign = -1;
		diffval(y.val, x.val, z.val);
		return z;
	}
}

cplx128 sqr(cplx128 z){
	int128 two = int128(1,0,0,0,(2 << 16));
	int128 r2 = mult128(z.r, z.r);
	int128 i2 = mult128(z.i, z.i);
	int128 z2r = sub128(r2, i2);
	return cplx128(z2r, mult128(mult128(two, z.r), z.i));
}

double magpow2(cplx128 z){
	double r = from128(z.r);
	double i = from128(z.i);

	return r * r + i * i;
}


// End of high precision

void checkGLError(int l)
{
	GLenum err;
	//	printf("Checking line %i\n", l);
	while ((err = glGetError()))
	{
		printf("GL error at line %i : %i\n", l, err);
	}
}

bool loadSettings(const char *settings_file_path)
{
	FILE* fp = fopen(settings_file_path, "rb"); // non-Windows use "r"
	char readBuffer[65536];
	FileReadStream is(fp, readBuffer, sizeof(readBuffer));
	Document settingsDoc;
	settingsDoc.ParseStream(is);
	fclose(fp);
		
	Value::ConstMemberIterator itr = settingsDoc.FindMember("minres");
	if (itr != settingsDoc.MemberEnd())
    	gMinRes = itr->value.GetInt();
	itr = settingsDoc.FindMember("maxthresh");
	if (itr != settingsDoc.MemberEnd())
    	gMaxThresh = itr->value.GetInt();
	itr = settingsDoc.FindMember("minthresh");
	if (itr != settingsDoc.MemberEnd())
    	gMinThresh = itr->value.GetInt();
	itr = settingsDoc.FindMember("framecap");
	if (itr != settingsDoc.MemberEnd())
    	gFrameCap = itr->value.GetInt();
	itr = settingsDoc.FindMember("movefraction");
	if (itr != settingsDoc.MemberEnd())
    	gMoveFraction = itr->value.GetDouble();
	itr = settingsDoc.FindMember("zoomfraction");
	if (itr != settingsDoc.MemberEnd())
    	gZoomFraction = itr->value.GetDouble();
	itr = settingsDoc.FindMember("threshfraction");
	if (itr != settingsDoc.MemberEnd())
    	gThreshFraction = itr->value.GetDouble();
	itr = settingsDoc.FindMember("huefraction");
	if (itr != settingsDoc.MemberEnd())
    	gHueFraction = itr->value.GetDouble();
	itr = settingsDoc.FindMember("huestep");
	if (itr != settingsDoc.MemberEnd())
    	gHueStep = itr->value.GetDouble();
	itr = settingsDoc.FindMember("winwidth");
	if (itr != settingsDoc.MemberEnd())
    	gWinWidth = itr->value.GetInt();
	itr = settingsDoc.FindMember("winheight");
	if (itr != settingsDoc.MemberEnd())
    	gWinHeight = itr->value.GetInt();
	itr = settingsDoc.FindMember("shaderpath");
	if (itr != settingsDoc.MemberEnd())
    	gShaderPath = itr->value.GetString();
	
	return true;
}

bool init()
{
	//Initialization flag
	bool success = true;

	loadSettings("src/settings.json");
	
	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		//Use OpenGL 3.1 core
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

		//Create window
		gGLWindow = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, gWinWidth, gWinHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
		if (gGLWindow == nullptr)
		{
			printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			//Create context
			gContext = SDL_GL_CreateContext(gGLWindow);
			if (gContext == nullptr)
			{
				printf("OpenGL context could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else
			{
#ifndef __APPLE__
				//Initialize GLEW
				glewExperimental = GL_TRUE;
				GLenum glewError = glewInit();
				if (glewError != GLEW_OK)
				{
					printf("Error initializing GLEW! %s\n", glewGetErrorString(glewError));
				}
#endif

				//Use Vsync
				if (SDL_GL_SetSwapInterval(1) < 0)
				{
					printf("Warning: Unable to set VSync! SDL Error: %s\n", SDL_GetError());
				}

				//Initialize OpenGL
				if (!initGL())
				{
					printf("Unable to initialize OpenGL!\n");
					success = false;
				}
			}
		}
	}

	return success;
}

bool initGL()
{
	//Success flag
	bool success = true;

	GLuint vao = 0;
	glGenVertexArrays(1, &vao);
	checkGLError(__LINE__);
	glBindVertexArray(vao);
	checkGLError(__LINE__);

	// Generate 1 buffer, put the resulting identifier in vertexbuffer
	glGenBuffers(1, &gVBO);
	checkGLError(__LINE__);

	glGenBuffers(1, &gVAB);
	checkGLError(__LINE__);

	glGenBuffers(1, &gTFB);
	checkGLError(__LINE__);

	glGenBuffers(1, &gIBO);
	checkGLError(__LINE__);

	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);
	checkGLError(__LINE__);

	gProgramID = LoadShaders(gShaderPath.c_str(), "src/fshader.txt");
	checkGLError(__LINE__);

	gParamsLocation = glGetUniformLocation(gProgramID, "params");
	checkGLError(__LINE__);
	if (gParamsLocation == -1)
	{
		fprintf(stderr, "Failed to get params locations\n");
		success = false;
		return success;
	}

	gThreshLocation = glGetUniformLocation(gProgramID, "maxI");
	checkGLError(__LINE__);
	if (gThreshLocation == -1)
	{
		fprintf(stderr, "Failed to get maxI locations\n");
		success = false;
		return success;
	}

	gColMapLocation = glGetUniformLocation(gProgramID, "colmap");
	checkGLError(__LINE__);
	if (gColMapLocation == -1)
	{
		fprintf(stderr, "Failed to get colour map locations\n");
		success = false;
		return success;
	}

	gParamValues = dvec4(-0.5, 0.0, 1.0, 1.0 * gWinHeight / gWinWidth);

	gThreshold = gMinThresh;

	gColMapValues = vec2(2.0 / 3.0, 3.0);

	return buildWinData();
}

bool buildWinData()
{
	bool success = true;

	gNoVertices = gWinWidth * gWinHeight * POINT_SCALE * POINT_SCALE;
	gNoCoords = gNoVertices * 2;

	if (gVBD != nullptr)
	{
		printf("About to delete gVBD\n");
		delete[] gVBD;
	}
	printf("About to create gVBD\n");
	gVBD = new GLfloat[gNoCoords];
	int i = 0;
	for (int y = 0; y < gWinHeight * POINT_SCALE; y++)
	{
		for (int x = 0; x < gWinWidth * POINT_SCALE; x++)
		{
			gVBD[i++] = GLfloat(-1.0 + x * (2.0 / gWinWidth / POINT_SCALE));
			gVBD[i++] = GLfloat(-1.0 + y * (2.0 / gWinHeight / POINT_SCALE));
		}
	}
	// The following commands will talk about our 'vertexbuffer' buffer
	glBindBuffer(GL_ARRAY_BUFFER, gVBO);
	checkGLError(__LINE__);
	// Give our vertices to OpenGL.
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * gNoCoords, gVBD, GL_STATIC_DRAW);
	checkGLError(__LINE__);

	if (gVAD != nullptr)
	{
		printf("About to delete gVAD\n");
		delete[] gVAD;
	}
	printf("About to create gVAD\n");
	gVAD = new GLint[gNoVertices];

	//				printf("About to clear VAD\n");
	for (int i = 0; i < gNoVertices; i++)
	{
		gVAD[i] = -1;
	}

	if (gTFD != nullptr)
	{
		printf("About to delete gTFD\n");
		delete[] gTFD;
	}
	printf("About to create gTFD\n");
	gTFD = new GLint[gNoVertices];

	return success;
}

bool handleWindow(SDL_WindowEvent w)
{
	if (w.event == SDL_WINDOWEVENT_SIZE_CHANGED)
	{
		gWinWidth = w.data1;
		gWinHeight = w.data2;
		gParamValues.w = gParamValues.z / gWinWidth * gWinHeight;
		glViewport(0, 0, gWinWidth, gWinHeight);
		buildWinData();
		printf("Finished sizing window event (%i,%i)\n", gWinWidth, gWinHeight);
		return true;
	}
	return false;
}

bool handleKeys(SDL_Keysym key, int x, int y)
{
	if (key.mod & KMOD_SHIFT)
	{
		if (key.sym == SDLK_UP)
		{
			gColMapValues.y += gHueStep;
			return true;
		}
		if (key.sym == SDLK_DOWN)
		{
			gColMapValues.y -= gHueStep;
			if (gColMapValues.y < 0)
				gColMapValues.y = 0;
			return true;
		}
		if (key.sym == SDLK_LEFT)
		{
			gColMapValues.x -= gHueFraction;
			return true;
		}
		if (key.sym == SDLK_RIGHT)
		{
			gColMapValues.x -= gHueFraction;
			if (gColMapValues.x < 0)
				gColMapValues.x = 1.0 - gColMapValues.x;
			return true;
		}
	}
	else
	{
		if (key.sym == SDLK_UP)
		{
			gParamValues.y += gParamValues.w * gMoveFraction;
			for (int i = 0; i < gNoVertices; i++)
			{
				gVAD[i] = -1;
			}
			return true;
		}
		if (key.sym == SDLK_DOWN)
		{
			gParamValues.y -= gParamValues.w * gMoveFraction;
			for (int i = 0; i < gNoVertices; i++)
			{
				gVAD[i] = -1;
			}
			return true;
		}
		if (key.sym == SDLK_LEFT)
		{
			gParamValues.x -= gParamValues.z * gMoveFraction;
			for (int i = 0; i < gNoVertices; i++)
			{
				gVAD[i] = -1;
			}
			return true;
		}
		if (key.sym == SDLK_RIGHT)
		{
			gParamValues.x += gParamValues.z * gMoveFraction;
			for (int i = 0; i < gNoVertices; i++)
			{
				gVAD[i] = -1;
			}
			return true;
		}
		if (key.sym == SDLK_i)
		{
			gParamValues.z *= (1.0 - gZoomFraction);
			gParamValues.w = gParamValues.z / gWinWidth * gWinHeight;
			for (int i = 0; i < gNoVertices; i++)
			{
				gVAD[i] = -1;
			}
			return true;
		}
		if (key.sym == SDLK_o)
		{
			gParamValues.z /= (1.0 - gZoomFraction);
			gParamValues.w = gParamValues.z / gWinWidth * gWinHeight;
			for (int i = 0; i < gNoVertices; i++)
			{
				gVAD[i] = -1;
			}
			return true;
		}
		if (key.sym == SDLK_d || key.sym == SDLK_h)
		{
			if (gThreshold == gMaxThresh)
				return 0;
			gThreshold *= 1.0 + gThreshFraction;
			if (gThreshold > gMaxThresh)
				gThreshold = gMaxThresh;
			printf("Max iterations = %i\n", gThreshold);
			for (int i = 0; i < gNoVertices; i++)
			{
				if (gVAD[i] == 0)
					gVAD[i] = -1;
			}
			return true;
		}
		if (key.sym == SDLK_s || key.sym == SDLK_l)
		{
			if (gThreshold == gMinThresh)
				return 0;
			gThreshold /= 1.0 + gThreshFraction;
			if (gThreshold < gMinThresh)
				gThreshold = gMinThresh;
			printf("Max iterations = %i\n", gThreshold);
			for (int i = 0; i < gNoVertices; i++)
			{
				if (gVAD[i] > gThreshold)
					gVAD[i] = 0;
			}
			return true;
		}
	}
	return false;
}

bool handleMouse(SDL_Event e)
{
	const Uint8 *state = SDL_GetKeyboardState(NULL);
	bool shiftKey = (state[SDL_SCANCODE_LSHIFT] == 1 || state[SDL_SCANCODE_RSHIFT] == 1);
	#ifdef __APPLE__
	int direction = -1;
	#else
	int direction = 1;
	#endif
	
//	if (e.wheel.which == SDL_TOUCH_MOUSEID) direction = -1;
//	printf("Which %i\n", e.wheel.which);
	if (e.type == SDL_MOUSEBUTTONUP)
	{
		SDL_MouseButtonEvent m = e.button;
		if (m.button == SDL_BUTTON_LEFT && m.clicks == 2)
		{
			gParamValues.x += (2.0 * m.x / gWinWidth - 1.0) * gParamValues.z;
			gParamValues.y += (1.0 - 2.0 * m.y / gWinHeight) * gParamValues.w;
			gParamValues.z *= (1.0 - gZoomFraction);
			gParamValues.w = gParamValues.z / gWinWidth * gWinHeight;
			for (int i = 0; i < gNoVertices; i++)
			{
				gVAD[i] = -1;
			}
			SDL_WarpMouseInWindow(gGLWindow, gWinWidth / 2, gWinHeight / 2);
			return true;
		}
		if (m.button == SDL_BUTTON_RIGHT && m.clicks == 2)
		{
			gParamValues.x += (2.0 * m.x / (gWinWidth)-1.0) * gParamValues.z;
			gParamValues.y += (1.0 - 2.0 * m.y / (gWinHeight)) * gParamValues.w;
			gParamValues.z /= (1.0 - gZoomFraction);
			gParamValues.w = gParamValues.z / gWinWidth * gWinHeight;
			for (int i = 0; i < gNoVertices; i++)
			{
				gVAD[i] = -1;
			}
			SDL_WarpMouseInWindow(gGLWindow, gWinWidth / 2, gWinHeight / 2);
			return true;
		}
	}
	if (e.type == SDL_MOUSEWHEEL)
	{

		if (shiftKey)
		{
			if (e.wheel.y * direction > 0)
			{
				if (gThreshold == gMaxThresh)
					return 0;
				gThreshold *= 1.0 + gThreshFraction;
				if (gThreshold > gMaxThresh)
					gThreshold = gMaxThresh;
				printf("Max iterations = %i\n", gThreshold);
				for (int i = 0; i < gNoVertices; i++)
				{
					if (gVAD[i] == 0)
						gVAD[i] = -1;
				}
				return true;
			}
			if (e.wheel.y *direction < 0)
			{
				if (gThreshold == gMinThresh)
					return 0;
				gThreshold /= 1.0 + gThreshFraction;
				if (gThreshold < gMinThresh)
					gThreshold = gMinThresh;
				printf("Max iterations = %i\n", gThreshold);
				for (int i = 0; i < gNoVertices; i++)
				{
					if (gVAD[i] > gThreshold)
						gVAD[i] = 0;
				}
				return true;
			}
			return false;
		}
		int x = 0, y = 0;
		SDL_GetMouseState(&x, &y);
		if (e.wheel.y *direction > 0)
		{
			gParamValues.x += (2.0 * x / gWinWidth - 1.0) * gParamValues.z;
			gParamValues.y += (1.0 - 2.0 * y / gWinHeight) * gParamValues.w;
			gParamValues.z *= (1.0 - gZoomFraction);
			gParamValues.w = gParamValues.z / gWinWidth * gWinHeight;
			for (int i = 0; i < gNoVertices; i++)
			{
				gVAD[i] = -1;
			}
			SDL_WarpMouseInWindow(gGLWindow, gWinWidth / 2, gWinHeight / 2);
			return true;
		}
		if (e.wheel.y * direction < 0)
		{
			gParamValues.x += (2.0 * x / (gWinWidth)-1.0) * gParamValues.z;
			gParamValues.y += (1.0 - 2.0 * y / (gWinHeight)) * gParamValues.w;
			gParamValues.z /= (1.0 - gZoomFraction);
			gParamValues.w = gParamValues.z / gWinWidth * gWinHeight;

			for (int i = 0; i < gNoVertices; i++)
			{
				gVAD[i] = -1;
			}
			SDL_WarpMouseInWindow(gGLWindow, gWinWidth / 2, gWinHeight / 2);
			return true;
		}
	}
	if (e.type == SDL_MOUSEMOTION)
	{
		if (shiftKey)
		{
			gColMapValues.y -= gHueStep * e.motion.yrel / 100.0;
			if (gColMapValues.y < 0)
				gColMapValues.y = 0;

			gColMapValues.x += gHueFraction * e.motion.xrel / 100.0;
			if (gColMapValues.x < 0)
				gColMapValues.x = 1.0 - gColMapValues.x;
			SDL_WarpMouseInWindow(gGLWindow, gWinWidth / 2, gWinHeight / 2);
			return true;
		}
		if (e.motion.state & SDL_BUTTON_LMASK > 0)
		{
			gParamValues.x += 2.0 * e.motion.xrel / gWinWidth * gParamValues.z * direction;
			gParamValues.y -= 2.0 * e.motion.yrel / gWinHeight * gParamValues.w * direction;
			gParamValues.w = gParamValues.z / gWinWidth * gWinHeight;
			for (int i = 0; i < gNoVertices; i++)
			{
				gVAD[i] = -1;
			}
			return true;
		}
	}

	return false;
}

void update()
{
	//No per frame update needed
}

void close()
{
	//Deallocate program
	glDeleteProgram(gProgramID);
	checkGLError(__LINE__);

	//Destroy window
	SDL_DestroyWindow(gGLWindow);
	gGLWindow = nullptr;

	//Quit SDL subsystems
	SDL_Quit();
}

int main(int argc, char *args[])
{
	int128 hpr = toInt128(3.3);
	int128 hpi = toInt128(0.25);
	int128 hpr2 = mult128(hpr, hpr);
	int128 hpi2 = mult128(hpi, hpi);
	int128 hpr2mi2 = sub128(hpr2, hpi2);
	int128 hp2ri = mult128(mult128(toInt128(2),hpr), hpi);
	cplx128 z = cplx128(hpr, hpi);
	cplx128 z2 = sqr(z);
	double z2r = from128(z2.r);
	double z2i = from128(z2.i);
	double mag = magpow2(z);
	double mag2 = magpow2(z2);


	double r = from128(hpr);
	double i = from128(hpi);
	double r2 = from128(hpr2);
	double i2 = from128(hpi2);
	double zr2 = from128(hpr2mi2);
	double zi2 = from128(hp2ri);

	printf("r %f, i %f, r2 %f, i2 %f, r2-i2 %f 2ri %f z2 %f+%fi \n", r, i, r2, i2, zr2, zi2, z2r, z2i);

	//Start up SDL and create window
	if (!init())
	{
		printf("Failed to initialize!\n");
		return 1;
	}
	else
	{
		//Main loop flag
		bool quit = false;

		int res = gMinRes, prevRes = 1;
		int noIndices = 0;

		//Event handler
		SDL_Event e;

		bool redraw = true;

		//		printf("Starting loop\n");
		//While application is running
		while (!quit)
		{
			if (res != prevRes)
			{
				int pointWidth = gWinWidth * POINT_SCALE / res;
				int pointHeight = gWinHeight * POINT_SCALE / res;
				noIndices = pointWidth * pointHeight;
				if (gIBD != nullptr)
				{
					//					printf("About to delete gIBD\n");
					delete[] gIBD;
				}
				//				printf("About to create gIBD\n");
				gIBD = new GLuint[noIndices];
				int i = 0;
				int ystep = gWinWidth * POINT_SCALE;
				//				printf ("About to set indices\n");
				for (int y = 0; y < pointHeight; y++)
				{
					int basey = y * res * ystep;
					for (int x = 0; x < pointWidth; x++)
					{
						int base = x * res + basey;
						if (i < noIndices)
						{
							gIBD[i++] = base;
						}
						else
						{
							printf("Indices %i, i %i\n", noIndices, i);
							printf("Overflow gIBD. Window (%i,%i) loop (%i,%i) res %i\n", gWinWidth, gWinHeight, x, y, res);
						}
						int parentX = (x / 2) * 2;
						int parentY = (y / 2) * 2;
						int parentB = parentX * res + (parentY * res * ystep);
						int parentI = gVAD[parentB];
						if (parentI > -1 && parentB != base)
						{
							bool same = true;
							for (int pY = -1; pY < 2; pY++)
							{
								for (int pX = -1; pX < 2; pX++)
								{
									int peerB = parentX + pX * res * 2 + (parentY + pY * res * 2) * ystep;
									if (peerB >= 0 && peerB < gNoVertices)
									{
										if (gVAD[peerB] != parentI)
										{
											same = false;
										}
									}
								}
							}
							if (same)
								if (base < gNoVertices)
								{
									gVAD[base] = parentI;
								}
								else
								{
									printf("Vertices %i, base %i\n", gNoVertices, base);
								}
						}
					}
				}
				//				printf("Have set indices\n");

				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gIBO);
				checkGLError(__LINE__);

				glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * noIndices, gIBD, GL_STATIC_DRAW);
				checkGLError(__LINE__);
			}

			int indstep = noIndices;
			GLint total_iter = noIndices * gThreshold;
			while (total_iter > gFrameCap && indstep > 6 && prevRes > 1)
			{
				total_iter /= 2;
				indstep /= 2;
			}
			bool haltLoop = false;
			for (int i = 0; i < noIndices && !quit && !haltLoop; i += indstep)
			{

				glUseProgram(gProgramID);
				checkGLError(__LINE__);

				// 1st attribute buffer : vertices
				glEnableVertexAttribArray(0);
				checkGLError(__LINE__);

				glBindBuffer(GL_ARRAY_BUFFER, gVBO);
				checkGLError(__LINE__);

				glVertexAttribPointer(
					0,		  // attribute 0. No particular reason for 0, but must match the layout in the shader.
					2,		  // size
					GL_FLOAT, // type
					GL_FALSE, // normalized?
					0,		  // stride
					(void *)0 // array buffer offset
				);
				checkGLError(__LINE__);

				glEnableVertexAttribArray(1);
				checkGLError(__LINE__);

				glBindBuffer(GL_ARRAY_BUFFER, gVAB);
				checkGLError(__LINE__);

				glBufferData(GL_ARRAY_BUFFER, sizeof(GLint) * gNoVertices, gVAD, GL_STATIC_DRAW);
				checkGLError(__LINE__);

				glVertexAttribIPointer(1, 1, GL_INT, 0, (void *)0);
				checkGLError(__LINE__);

				glBindBuffer(GL_ARRAY_BUFFER, gTFB);
				checkGLError(__LINE__);

				glBufferData(GL_ARRAY_BUFFER, sizeof(GLint) * gNoVertices, nullptr, GL_STATIC_READ);
				checkGLError(__LINE__);

				glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, gTFB);
				checkGLError(__LINE__);

				glUniform4d(gParamsLocation, gParamValues.x, gParamValues.y, gParamValues.z, gParamValues.w);
				checkGLError(__LINE__);

				glUniform1i(gThreshLocation, gThreshold);
				checkGLError(__LINE__);

				glUniform2f(gColMapLocation, gColMapValues.x, gColMapValues.y);
				checkGLError(__LINE__);

				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gIBO);
				checkGLError(__LINE__);

				GLuint query;
				glGenQueries(1, &query);
				checkGLError(__LINE__);

				glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query);
				checkGLError(__LINE__);

				glBeginTransformFeedback(GL_POINTS);
				checkGLError(__LINE__);

				glPointSize(res);
				checkGLError(__LINE__);

				int noElements = indstep;
				if (i+noElements >= noIndices) noElements = noIndices - i;

				glDrawElements(GL_POINTS, noElements, GL_UNSIGNED_INT, (void *)(i * sizeof(GLuint)));
				checkGLError(__LINE__);

				glEndTransformFeedback();
				checkGLError(__LINE__);

				glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);
				checkGLError(__LINE__);

				GLuint primitives;
				glGetQueryObjectuiv(query, GL_QUERY_RESULT, &primitives);
				checkGLError(__LINE__);

				//				printf("i %i, Primitives %i, resolution %i\n", i, primitives, res);

				glDisableVertexAttribArray(0);
				checkGLError(__LINE__);

				glDisableVertexAttribArray(1);
				checkGLError(__LINE__);

				//Update screen
				SDL_GL_SwapWindow(gGLWindow);

				glGetBufferSubData(GL_TRANSFORM_FEEDBACK_BUFFER, 0, sizeof(GLint) * gNoVertices, gTFD);
				checkGLError(__LINE__);

				glReadBuffer(GL_FRONT);
				checkGLError(__LINE__);

				glDrawBuffer(GL_BACK);
				checkGLError(__LINE__);

				glBlitFramebuffer(0, 0, gWinWidth, gWinHeight,
								  0, 0, gWinWidth, gWinHeight, GL_COLOR_BUFFER_BIT, GL_NEAREST);
				checkGLError(__LINE__);

				//Update screen
				/* SDL_GL_SwapWindow(gGLWindow);

				glReadBuffer(GL_BACK);
				checkGLError(__LINE__);

				glDrawBuffer(GL_FRONT);
				checkGLError(__LINE__); */

				if (primitives + i > noIndices)
				{
					printf("Primitives %i, i %i, indices %i\n", primitives, i, noIndices);
				}
				else
				{
					for (int p = 0; p < primitives; p++)
					{
						if (gIBD[i + p] < gNoVertices)
						{
							if (gVAD[gIBD[i + p]] < 0)
								gVAD[gIBD[i + p]] = gTFD[p];
						}
						else
						{
							printf("i %i, p %i, gIBD %i, vertices %i\n", i, p, gIBD[i + p], gNoVertices);
						}
					}
				}

				redraw = false;
				while (SDL_PollEvent(&e) != 0 && !quit)
				{
					//User requests quit
					if (e.type == SDL_QUIT)
					{
						quit = true;
					}
					else if (e.type == SDL_KEYDOWN)
					{
						if (e.key.keysym.sym == SDLK_q)
						{
							quit = true;
						}
					}
					else if (e.type == SDL_KEYUP)
					{
						int x = 0, y = 0;
						SDL_GetMouseState(&x, &y);
						redraw = redraw || handleKeys(e.key.keysym, x, y);
					}
					else if (e.type == SDL_WINDOWEVENT)
					{
						redraw = redraw || handleWindow(e.window);
					}
					else if (e.type == SDL_MOUSEMOTION || e.type == SDL_MOUSEWHEEL || e.type == SDL_MOUSEWHEEL || e.type == SDL_MOUSEBUTTONUP || e.type == SDL_MOUSEBUTTONDOWN)
						redraw = redraw || handleMouse(e);
				}
				if (redraw) haltLoop = true;
			}
			if (haltLoop) {
				res = gMinRes;
			} else {
				if (prevRes != res) 
					if (res == 1) printf("Finished. centre (%.10e,%.10e) radius %.10e\n", gParamValues.x, gParamValues.y, (gParamValues.z + gParamValues.w)/2.0);
					else printf("Finished frame at resolution %i\n", res);
				prevRes = res;
				if (res > 1) res /= 2;
			}
		}

		//Free resources and close SDL
		close();

		return 0;
	}
}

GLuint LoadShaders(const char *vertex_file_path, const char *fragment_file_path)
{

	// Create the shaders
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	// Read the Vertex Shader code from the file
	std::string VertexShaderCode;
	std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
	if (VertexShaderStream.is_open())
	{
		std::stringstream sstr;
		sstr << VertexShaderStream.rdbuf();
		VertexShaderCode = sstr.str();
		VertexShaderStream.close();
	}
	else
	{
		printf("Impossible to open %s. Are you in the right directory ? Don't forget to read the FAQ !\n", vertex_file_path);
		getchar();
		return 0;
	}

	// Read the Fragment Shader code from the file
	std::string FragmentShaderCode;
	std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
	if (FragmentShaderStream.is_open())
	{
		std::stringstream sstr;
		sstr << FragmentShaderStream.rdbuf();
		FragmentShaderCode = sstr.str();
		FragmentShaderStream.close();
	}

	GLint Result = GL_FALSE;
	int InfoLogLength;

	// Compile Vertex Shader
	printf("Compiling shader : %s\n", vertex_file_path);
	char const *VertexSourcePointer = VertexShaderCode.c_str();
	glShaderSource(VertexShaderID, 1, &VertexSourcePointer, NULL);
	glCompileShader(VertexShaderID);

	// Check Vertex Shader
	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0)
	{
		std::vector<char> VertexShaderErrorMessage(InfoLogLength + 1);
		glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
		printf("%s\n", &VertexShaderErrorMessage[0]);
	}

	// Compile Fragment Shader
	printf("Compiling shader : %s\n", fragment_file_path);
	char const *FragmentSourcePointer = FragmentShaderCode.c_str();
	glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer, NULL);
	glCompileShader(FragmentShaderID);

	// Check Fragment Shader
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0)
	{
		std::vector<char> FragmentShaderErrorMessage(InfoLogLength + 1);
		glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
		printf("%s\n", &FragmentShaderErrorMessage[0]);
	}

	// Link the program
	printf("Linking program\n");
	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);

	const GLchar *feedbackVaryings[] = {"outValue"};
	glTransformFeedbackVaryings(ProgramID, 1, feedbackVaryings, GL_INTERLEAVED_ATTRIBS);

	glLinkProgram(ProgramID);

	// Check the program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0)
	{
		std::vector<char> ProgramErrorMessage(InfoLogLength + 1);
		glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
		printf("%s\n", &ProgramErrorMessage[0]);
	}

	glDetachShader(ProgramID, VertexShaderID);
	glDetachShader(ProgramID, FragmentShaderID);

	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	return ProgramID;
}
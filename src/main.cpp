#define GL_SILENCE_DEPRECATION

#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <sstream>
using namespace std;

#include <stdlib.h>
#include <string.h>
#include <SDL.h>

#ifdef __APPLE__
#include <OpenGL/gl3.h>
#include <OpenGL/glu.h>
#include <glm/glm.hpp>
#else
#include <GL\glew.h>
#include <GL\gl.h>
#include <GL\glu.h>
#include <glm\glm.hpp>
#endif

using namespace glm;

#include <SDL_opengl.h>

#include <rapidjson/document.h>
#include "rapidjson/filereadstream.h"
#include <cstdio>

using namespace rapidjson;

#ifdef __APPLE__
#define POINT_SCALE 1
#else
#define POINT_SCALE 1
#endif
int gMinRes = 128;
int gMaxThresh = 1000000;
int gMinThresh = 1000;
int gFrameCap = 25000 * 1024;
int gHighCapFactor = 8;

double gMoveFraction = 1.0/64/0;
double gZoomFraction = 0.05;
double gThreshFraction = 0.25;
double gHueFraction = 1.0/12.0;
double gHueStep = 0.5;

//Screen dimension constants
int gWinWidth = 1024;
int gWinHeight = 512;

void checkGLError(int l);

GLuint LoadShaders(bool feedback, const char * vertex_file_path,const char * fragment_file_path);

//Starts up SDL, creates window, and initializes OpenGL
bool init();

//Initializes rendering program and clear color
bool initGL();

bool handleWindow(SDL_WindowEvent w);

bool buildWinData();

//Input handler
bool handleKeys(unsigned char key, int x, int y);

//Per frame update
void update();

//Renders quad to the screen
void render();

//Frees media and shuts down SDL
void close();

//Shader loading utility programs
void printProgramLog(GLuint program);
void printShaderLog(GLuint shader);

//The window we'll be rendering to
SDL_Window *gGLWindow = nullptr;

//OpenGL context
SDL_GLContext gContext;

//Graphics program
GLuint gTextureVA = 0;
GLuint gScreenVA = 0;
int gProgram = 0;
GLuint gProgramID[] = {0,0};
GLuint gScreenPID = 0;
GLint gCentreLocation = -1;
GLint gScaleLocation = -1;
GLint gColMapLocation = -1;
GLint gParamsLocation = -1;
GLint gScrParamLocation = -1;
GLint gTextureLocation = -1;
GLint gInTexLocation = -1;

GLuint gVBO = 0;
GLuint gScreenBO = 0;
GLuint gIBO = 0;
GLuint gVAB = 0;
GLuint gTFB = 0;
GLuint gFramebuffer = 0;
int gOutTexture = 0;
GLuint gTexture[] = {0,0};
GLenum gDrawBuffers[] = {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
GLint gTexEmpty[] = {-1,-1,-1,-1};
dvec2 gCentrer = dvec2(-0.5, 0.0);
dvec2 gCentrei = dvec2(0.0, 0.0);
dvec2 gScaler = dvec2(1.0, 0.0);
dvec2 gScalei = dvec2(1.0, 0.0);
vec2 gColMapValues = vec2(2.0 / 3.0, 3.0);

GLint gThreshold = 0;
string gLowShader = "";
string gHighShader = "";

int gNoVertices = 0;
int gNoCoords = 0;
GLint *gVBD = nullptr;
/* GLint *gVAD = nullptr;
GLint *gTFD = nullptr; */
GLuint *gIBD = nullptr;

static const GLfloat gScreenBD[] = {
    -1.0f, -1.0f,
    1.0f, -1.0f,
    -1.0f,  1.0f,
    -1.0f,  1.0f,
    1.0f, -1.0f,
    1.0f,  1.0f
};


//start of high precision
dvec2 qadd (dvec2 dsa, dvec2 dsb)
{
dvec2 dsc;
double t1, t2, e;

t1 = dsa.x + dsb.x;
e = t1 - dsa.x;
t2 = ((dsb.x - e) + (dsa.x - (t1 - e))) + dsa.y + dsb.y;

dsc.x = t1 + t2;
dsc.y = t2 - (dsc.x - t1);
return dsc;
}

dvec2 qmul (dvec2 dsa, dvec2 dsb)
{
dvec2 dsc;
double c11, c21, c2, e, t1, t2;
double a1, a2, b1, b2, cona, conb, split = 536870913.;

cona = dsa.x * split;
conb = dsb.x * split;
a1 = cona - (cona - dsa.x);
b1 = conb - (conb - dsb.x);
a2 = dsa.x - a1;
b2 = dsb.x - b1;

c11 = dsa.x * dsb.x;
c21 = a2 * b2 + (a2 * b1 + (a1 * b2 + (a1 * b1 - c11)));

c2 = dsa.x * dsb.y + dsa.y * dsb.x;

t1 = c11 + c2;
e = t1 - c11;
t2 = dsa.y * dsb.y + ((c2 - e) + (c11 - (t1 - e))) + c21;

dsc.x = t1 + t2;
dsc.y = t2 - (dsc.x - t1);

return dsc;
}

void checkGLError(int l)
{
	GLenum err;
	//	printf("Checking line %i\n", l);
	while ((err = glGetError()))
	{
		printf("GL error at line %i : %i\n", l, err);
	}
}

bool loadSettings(const char *settings_file_path)
{
	FILE* fp = fopen(settings_file_path, "rb"); // non-Windows use "r"
	char readBuffer[65536];
	FileReadStream is(fp, readBuffer, sizeof(readBuffer));
	Document settingsDoc;
	settingsDoc.ParseStream(is);
	fclose(fp);
		
	Value::ConstMemberIterator itr = settingsDoc.FindMember("minres");
	if (itr != settingsDoc.MemberEnd())
    	gMinRes = itr->value.GetInt();
	itr = settingsDoc.FindMember("highcapfactor");
	if (itr != settingsDoc.MemberEnd())
		gHighCapFactor = itr->value.GetInt();
	itr = settingsDoc.FindMember("maxthresh");
	if (itr != settingsDoc.MemberEnd())
    	gMaxThresh = itr->value.GetInt();
	itr = settingsDoc.FindMember("minthresh");
	if (itr != settingsDoc.MemberEnd())
    	gMinThresh = itr->value.GetInt();
	itr = settingsDoc.FindMember("framecap");
	if (itr != settingsDoc.MemberEnd())
    	gFrameCap = itr->value.GetInt();
	itr = settingsDoc.FindMember("movefraction");
	if (itr != settingsDoc.MemberEnd())
    	gMoveFraction = itr->value.GetDouble();
	itr = settingsDoc.FindMember("zoomfraction");
	if (itr != settingsDoc.MemberEnd())
    	gZoomFraction = itr->value.GetDouble();
	itr = settingsDoc.FindMember("threshfraction");
	if (itr != settingsDoc.MemberEnd())
    	gThreshFraction = itr->value.GetDouble();
	itr = settingsDoc.FindMember("huefraction");
	if (itr != settingsDoc.MemberEnd())
    	gHueFraction = itr->value.GetDouble();
	itr = settingsDoc.FindMember("huestep");
	if (itr != settingsDoc.MemberEnd())
    	gHueStep = itr->value.GetDouble();
	itr = settingsDoc.FindMember("winwidth");
	if (itr != settingsDoc.MemberEnd())
	{
    	gWinWidth = itr->value.GetInt();
		gScalei = qmul(gScaler, dvec2((double)gWinHeight/ gWinWidth, 0.));
	}
	itr = settingsDoc.FindMember("winheight");
	if (itr != settingsDoc.MemberEnd())
	{
    	gWinHeight = itr->value.GetInt();
		gScalei = qmul(gScaler, dvec2((double)gWinHeight/ gWinWidth, 0.));
	}
	itr = settingsDoc.FindMember("lowshader");
	if (itr != settingsDoc.MemberEnd())
    	gLowShader = itr->value.GetString();	
	itr = settingsDoc.FindMember("highshader");
	if (itr != settingsDoc.MemberEnd())
    	gHighShader = itr->value.GetString();	
	itr = settingsDoc.FindMember("r");
	if (itr != settingsDoc.MemberEnd())
	{
    	gCentrer = dvec2(itr->value.GetDouble(), 0.);
	}
	itr = settingsDoc.FindMember("i");
	if (itr != settingsDoc.MemberEnd())
	{
    	gCentrei = dvec2(itr->value.GetDouble(), 0.);
	}
	itr = settingsDoc.FindMember("scale");
	if (itr != settingsDoc.MemberEnd())
	{
    	gScaler = dvec2(itr->value.GetDouble(), 0.);
		gScalei = qmul(gScaler, dvec2((double)gWinHeight/ gWinWidth, 0.));

	}
	itr = settingsDoc.FindMember("basehue");
	if (itr != settingsDoc.MemberEnd())
    	gColMapValues.x = itr->value.GetDouble();
	itr = settingsDoc.FindMember("huescale");
	if (itr != settingsDoc.MemberEnd())
    	gColMapValues.y = itr->value.GetDouble();

	return true;
}

bool init()
{
	//Initialization flag
	bool success = true;

	loadSettings("src/settings.json");
	
	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		//Use OpenGL 3.1 core
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

		//Create window
		gGLWindow = SDL_CreateWindow("Mandel", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, gWinWidth, gWinHeight, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
		if (gGLWindow == nullptr)
		{
			printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			//Create context
			gContext = SDL_GL_CreateContext(gGLWindow);
			if (gContext == nullptr)
			{
				printf("OpenGL context could not be created! SDL Error: %s\n", SDL_GetError());
				success = false;
			}
			else
			{
#ifndef __APPLE__
				//Initialize GLEW
				glewExperimental = GL_TRUE;
				GLenum glewError = glewInit();
				if (glewError != GLEW_OK)
				{
					printf("Error initializing GLEW! %s\n", glewGetErrorString(glewError));
				}
#endif

				//Use Vsync
				if (SDL_GL_SetSwapInterval(1) < 0)
				{
					printf("Warning: Unable to set VSync! SDL Error: %s\n", SDL_GetError());
				}

				//Initialize OpenGL
				if (!initGL())
				{
					printf("Unable to initialize OpenGL!\n");
					success = false;
				}
			}
		}
	}

	return success;
}

bool initGL()
{
	//Success flag
	bool success = true;

	glGenVertexArrays(1, &gTextureVA);
	checkGLError(__LINE__);

	glBindVertexArray(gTextureVA);
	checkGLError(__LINE__);
	
	// Generate 1 buffer, put the resulting identifier in vertexbuffer
	glGenBuffers(1, &gVBO);
	checkGLError(__LINE__);

	glGenBuffers(1, &gVAB);
	checkGLError(__LINE__);

	glGenBuffers(1, &gTFB);
	checkGLError(__LINE__);

	glGenBuffers(1, &gIBO);
	checkGLError(__LINE__);

	glGenTextures(2, gTexture);
	checkGLError(__LINE__);

	glBindTexture(GL_TEXTURE_2D, gTexture[0]);
	checkGLError(__LINE__);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32I, gWinWidth, gWinHeight, 0,GL_RED_INTEGER, GL_UNSIGNED_BYTE, 0);
	checkGLError(__LINE__);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	checkGLError(__LINE__);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	checkGLError(__LINE__);

	glBindTexture(GL_TEXTURE_2D, gTexture[1]);
	checkGLError(__LINE__);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_R32I, gWinWidth, gWinHeight, 0,GL_RED_INTEGER, GL_UNSIGNED_BYTE, 0);
	checkGLError(__LINE__);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	checkGLError(__LINE__);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	checkGLError(__LINE__);

	glGenFramebuffers(1, &gFramebuffer);
	checkGLError(__LINE__);

	glBindFramebuffer(GL_FRAMEBUFFER, gFramebuffer);
	checkGLError(__LINE__);

	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, gTexture[0], 0);
	checkGLError(__LINE__);

	glDrawBuffer(GL_COLOR_ATTACHMENT0);
	glClearBufferiv(GL_COLOR, 0, gTexEmpty);
	checkGLError(__LINE__);

	glDrawBuffers(2, gDrawBuffers);
	checkGLError(__LINE__);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) return false;

	glBindFramebuffer(GL_FRAMEBUFFER, gFramebuffer);
	glViewport(0,0,gWinWidth,gWinHeight);

	glGenVertexArrays(1, &gScreenVA);
	checkGLError(__LINE__);

	glBindVertexArray(gScreenVA);
	checkGLError(__LINE__);

	glGenBuffers(1, &gScreenBO);
	checkGLError(__LINE__);

	glBindBuffer(GL_ARRAY_BUFFER, gScreenBO);
	checkGLError(__LINE__);

	glBufferData(GL_ARRAY_BUFFER, sizeof(gScreenBD), gScreenBD, GL_STATIC_DRAW);
	checkGLError(__LINE__);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0,0,gWinWidth,gWinHeight);

	glEnableVertexAttribArray(0);
	checkGLError(__LINE__);

	glBindBuffer(GL_ARRAY_BUFFER, gScreenBO);
	checkGLError(__LINE__);

	glVertexAttribPointer(
					0,		  // attribute 0. No particular reason for 0, but must match the layout in the shader.
					2,		  // size
					GL_FLOAT, // type
					GL_FALSE, // normalized?
					0,		  // stride
					(void *)0 // array buffer offset
				);
	checkGLError(__LINE__);

	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);

	gScreenPID = LoadShaders(false, "src/passthroughshader.txt", "src/textureshader.txt");

	gTextureLocation = glGetUniformLocation(gScreenPID, "tex");
	checkGLError(__LINE__);
	if (gTextureLocation == -1)
	{
		fprintf(stderr, "Failed to get texture location\n");
		success = false;
		return success;
	}
	checkGLError(__LINE__);

	gScrParamLocation = glGetUniformLocation(gScreenPID, "params");
	checkGLError(__LINE__);
	if (gScrParamLocation == -1)
	{
		fprintf(stderr, "Failed to get params locations\n");
		success = false;
		return success;
	}

	gColMapLocation = glGetUniformLocation(gScreenPID, "colmap");
	checkGLError(__LINE__);
	if (gColMapLocation == -1)
	{
		fprintf(stderr, "Failed to get colour map locations\n");
		success = false;
		return success;
	}

	gProgramID[0] = LoadShaders(true, gLowShader.c_str(), "src/tfshader.txt");
	checkGLError(__LINE__);

	gProgramID[1] = LoadShaders(true, gHighShader.c_str(), "src/tfshader.txt");
	checkGLError(__LINE__);

	gCentreLocation = glGetUniformLocation(gProgramID[0], "centre");
	checkGLError(__LINE__);
	if (gCentreLocation == -1)
	{
		fprintf(stderr, "Failed to get centre location\n");
		success = false;
		return success;
	}

	gScaleLocation = glGetUniformLocation(gProgramID[0], "scale");
	checkGLError(__LINE__);
	if (gScaleLocation == -1)
	{
		fprintf(stderr, "Failed to get scale locations\n");
		success = false;
		return success;
	}

	gParamsLocation = glGetUniformLocation(gProgramID[0], "params");
	checkGLError(__LINE__);
	if (gParamsLocation == -1)
	{
		fprintf(stderr, "Failed to get params locations\n");
		success = false;
		return success;
	}

/*	gWidthLocation = glGetUniformLocation(gProgramID[0], "width");
	checkGLError(__LINE__);
	if (gWidthLocation == -1)
	{
		fprintf(stderr, "Failed to get width locations\n");
		success = false;
		return success;
	}

	gHeightLocation = glGetUniformLocation(gProgramID[0], "height");
	checkGLError(__LINE__);
	if (gHeightLocation == -1)
	{
		fprintf(stderr, "Failed to get height locations\n");
		success = false;
		return success;
	} */

	gInTexLocation = glGetUniformLocation(gProgramID[0], "inTex");
	checkGLError(__LINE__);
	if (gInTexLocation == -1)
	{
		fprintf(stderr, "Failed to get inTex location\n");
		success = false;
		return success;
	}
	checkGLError(__LINE__);

	gThreshold = gMinThresh;

	return buildWinData();
}

bool buildWinData()
{
	bool success = true;

	gNoVertices = gWinWidth * gWinHeight * POINT_SCALE * POINT_SCALE;
	gNoCoords = gNoVertices * 2;

	if (gVBD != nullptr)
	{
		printf("About to delete gVBD\n");
		delete[] gVBD;
	}
	printf("About to create gVBD\n");
	gVBD = new GLint[gNoCoords];
	int i = 0;
	for (int y = 0; y < gWinHeight * POINT_SCALE; y++)
	{
		for (int x = 0; x < gWinWidth * POINT_SCALE; x++)
		{
			gVBD[i++] = x;
			gVBD[i++] = y;
		}
	}
	// The following commands will talk about our 'vertexbuffer' buffer
	glBindBuffer(GL_ARRAY_BUFFER, gVBO);
	checkGLError(__LINE__);
	// Give our vertices to OpenGL.
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLint) * gNoCoords, gVBD, GL_STATIC_DRAW);
	checkGLError(__LINE__);

/*	if (gVAD != nullptr)
	{
		printf("About to delete gVAD\n");
		delete[] gVAD;
	}
	printf("About to create gVAD\n");
	gVAD = new GLint[gNoVertices];

	//				printf("About to clear VAD\n");
	for (int i = 0; i < gNoVertices; i++)
	{
		gVAD[i] = -1;
	} */
	glBindFramebuffer(GL_FRAMEBUFFER, gFramebuffer);
	glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, gTexture[gOutTexture], 0);
	glDrawBuffers(2, gDrawBuffers);
	glClearBufferiv(GL_COLOR, 1, gTexEmpty);
	checkGLError(__LINE__);

/*	if (gTFD != nullptr)
	{
		printf("About to delete gTFD\n");
		delete[] gTFD;
	}
	printf("About to create gTFD\n");
	gTFD = new GLint[gNoVertices]; */

	return success;
}

bool handleWindow(SDL_WindowEvent w)
{
	if (w.event == SDL_WINDOWEVENT_SIZE_CHANGED)
	{
		gWinWidth = w.data1;
		gWinHeight = w.data2;
		gScalei = qmul(gScaler, dvec2((double)gWinHeight/ gWinWidth, 0.));

		glBindTexture(GL_TEXTURE_2D, gTexture[0]);
		checkGLError(__LINE__);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_R32I, gWinWidth, gWinHeight, 0,GL_RED_INTEGER, GL_UNSIGNED_BYTE, 0);
		checkGLError(__LINE__);

		glBindTexture(GL_TEXTURE_2D, gTexture[1]);
		checkGLError(__LINE__);

		glTexImage2D(GL_TEXTURE_2D, 0, GL_R32I, gWinWidth, gWinHeight, 0,GL_RED_INTEGER, GL_UNSIGNED_BYTE, 0);
		checkGLError(__LINE__);

		glBindFramebuffer(GL_FRAMEBUFFER, gFramebuffer);
		glViewport(0,0,gWinWidth,gWinHeight);

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0, 0, gWinWidth, gWinHeight);

		buildWinData();

		printf("Finished sizing window event (%i,%i)\n", gWinWidth, gWinHeight);
		return true;
	}
	return false;
}

bool handleKeys(SDL_Keysym key, int x, int y)
{
	if (key.mod & KMOD_SHIFT)
	{
		if (key.sym == SDLK_UP)
		{
			gColMapValues.y += gHueStep;
			return true;
		}
		if (key.sym == SDLK_DOWN)
		{
			gColMapValues.y -= gHueStep;
			if (gColMapValues.y < 0)
				gColMapValues.y = 0;
			return true;
		}
		if (key.sym == SDLK_LEFT)
		{
			gColMapValues.x -= gHueFraction;
			return true;
		}
		if (key.sym == SDLK_RIGHT)
		{
			gColMapValues.x -= gHueFraction;
			if (gColMapValues.x < 0)
				gColMapValues.x = 1.0 - gColMapValues.x;
			return true;
		}
		return false;
	}
	else
	{
		if (key.sym == SDLK_UP)
		{

			gCentrei = qadd(gCentrei, qmul(gScalei, dvec2(gMoveFraction, 0.)));
/*			for (int i = 0; i < gNoVertices; i++)
			{
				gVAD[i] = -1;
			}*/
			glBindFramebuffer(GL_FRAMEBUFFER, gFramebuffer);
			glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, gTexture[gOutTexture], 0);
			glDrawBuffers(2, gDrawBuffers);
			glClearBufferiv(GL_COLOR, 1, gTexEmpty);
			checkGLError(__LINE__);

			return true;
		}
		if (key.sym == SDLK_DOWN)
		{
			gCentrei = qadd(gCentrei, -qmul(gScalei, dvec2(gMoveFraction, 0.)));
/*			for (int i = 0; i < gNoVertices; i++)
			{
				gVAD[i] = -1;
			}*/
			glBindFramebuffer(GL_FRAMEBUFFER, gFramebuffer);
			glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, gTexture[gOutTexture], 0);
			glDrawBuffers(2, gDrawBuffers);
			glClearBufferiv(GL_COLOR, 1, gTexEmpty);
			checkGLError(__LINE__);

			return true;
		}
		if (key.sym == SDLK_LEFT)
		{
			gCentrer = qadd(gCentrer, -qmul(gScaler, dvec2(gMoveFraction, 0.)));
/*			for (int i = 0; i < gNoVertices; i++)
			{
				gVAD[i] = -1;
			}*/
			glBindFramebuffer(GL_FRAMEBUFFER, gFramebuffer);
			glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, gTexture[gOutTexture], 0);
			glDrawBuffers(2, gDrawBuffers);
			glClearBufferiv(GL_COLOR, 1, gTexEmpty);
			checkGLError(__LINE__);

			return true;
		}
		if (key.sym == SDLK_RIGHT)
		{
			gCentrer = qadd(gCentrer, qmul(gScaler, dvec2(gMoveFraction, 0.)));
/*			for (int i = 0; i < gNoVertices; i++)
			{
				gVAD[i] = -1;
			}*/
			glBindFramebuffer(GL_FRAMEBUFFER, gFramebuffer);
			glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, gTexture[gOutTexture], 0);
			glDrawBuffers(2, gDrawBuffers);
			glClearBufferiv(GL_COLOR, 1, gTexEmpty);
			checkGLError(__LINE__);

			return true;
		}
		if (key.sym == SDLK_i)
		{
			gScaler = qmul(gScaler, dvec2(1.0 - gMoveFraction, 0.));
			gScalei = qmul(gScaler, dvec2((double)gWinHeight/gWinWidth, 0.));
/*			for (int i = 0; i < gNoVertices; i++)
			{
				gVAD[i] = -1;
			}*/
			glBindFramebuffer(GL_FRAMEBUFFER, gFramebuffer);
			glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, gTexture[gOutTexture], 0);
			glDrawBuffers(2, gDrawBuffers);
			glClearBufferiv(GL_COLOR, 1, gTexEmpty);
			checkGLError(__LINE__);

			return true;
		}
		if (key.sym == SDLK_o)
		{
			gScaler = qmul(gScaler, dvec2(1.0 + gMoveFraction, 0.));
			gScalei = qmul(gScaler, dvec2((double)gWinHeight/gWinWidth, 0.));
/*			for (int i = 0; i < gNoVertices; i++)
			{
				gVAD[i] = -1;
			}*/
			glBindFramebuffer(GL_FRAMEBUFFER, gFramebuffer);
			glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, gTexture[gOutTexture], 0);
			glDrawBuffers(2, gDrawBuffers);
			glClearBufferiv(GL_COLOR, 1, gTexEmpty);
			checkGLError(__LINE__);

			return true;
		}
		if (key.sym == SDLK_d || key.sym == SDLK_h)
		{
			if (gThreshold == gMaxThresh)
				return false;
			gThreshold *= 1.0 + gThreshFraction;
			if (gThreshold > gMaxThresh)
				gThreshold = gMaxThresh;
//	printf("\rMax ierations = %i\n", gThreshold);
/*			for (int i = 0; i < gNoVertices; i++)
			{
//				if (gVAD[i] == 0)
					gVAD[i] = -1;
			}*/
			glBindFramebuffer(GL_FRAMEBUFFER, gFramebuffer);
			glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, gTexture[gOutTexture], 0);
			glDrawBuffers(2, gDrawBuffers);
			glClearBufferiv(GL_COLOR, 1, gTexEmpty);
			checkGLError(__LINE__);

			return true;
		}
		if (key.sym == SDLK_s || key.sym == SDLK_l)
		{
			if (gThreshold == gMinThresh)
				return false;
			gThreshold /= 1.0 + gThreshFraction;
			if (gThreshold < gMinThresh)
				gThreshold = gMinThresh;
//	printf("Max iterations = %i\n", gThreshold);
/*			for (int i = 0; i < gNoVertices; i++)
			{
				if (gVAD[i] > gThreshold)
					gVAD[i] = 0;
					gVAD[i] = -1;
			}*/
			glBindFramebuffer(GL_FRAMEBUFFER, gFramebuffer);
			glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, gTexture[gOutTexture], 0);
			glDrawBuffers(2, gDrawBuffers);
			glClearBufferiv(GL_COLOR, 1, gTexEmpty);
			checkGLError(__LINE__);

			return true;
		}
		if (key.sym == SDLK_p)
		{
			gProgram = 1-gProgram;
			
			gCentreLocation = glGetUniformLocation(gProgramID[gProgram], "centre");
			checkGLError(__LINE__);
			if (gCentreLocation == -1)
			{
				fprintf(stderr, "Failed to get centre location\n");
				return false;
			}

			gScaleLocation = glGetUniformLocation(gProgramID[gProgram], "scale");
			checkGLError(__LINE__);
			if (gScaleLocation == -1)
			{
				fprintf(stderr, "Failed to get scale locations\n");
				return false;
			}

			gParamsLocation = glGetUniformLocation(gProgramID[gProgram], "params");
			checkGLError(__LINE__);
			if (gParamsLocation == -1)
			{
				fprintf(stderr, "Failed to get params locations\n");
				return false;
			}

/*			gWidthLocation = glGetUniformLocation(gProgramID[gProgram], "width");
			checkGLError(__LINE__);
			if (gWidthLocation == -1)
			{
				fprintf(stderr, "Failed to get width locations\n");
				return false;
			}

			gHeightLocation = glGetUniformLocation(gProgramID[gProgram], "height");
			checkGLError(__LINE__);
			if (gHeightLocation == -1)
			{
				fprintf(stderr, "Failed to get height locations\n");
				return false;
			} */

			gInTexLocation = glGetUniformLocation(gProgramID[gProgram], "inTex");
			checkGLError(__LINE__);
			if (gInTexLocation == -1)
			{
				fprintf(stderr, "Failed to get inTex location\n");
				return false;
			}
			checkGLError(__LINE__);

			return true;
		}
		return false;
	}
}

bool handleMouse(SDL_Event e)
{
	const Uint8 *state = SDL_GetKeyboardState(NULL);
	bool shiftKey = (state[SDL_SCANCODE_LSHIFT] == 1 || state[SDL_SCANCODE_RSHIFT] == 1);
	#ifdef __APPLE__
	int direction = -1;
	#else
	int direction = 1;
	#endif
	
//	if (e.wheel.which == SDL_TOUCH_MOUSEID) direction = -1;
//	printf("Which %i\n", e.wheel.which);
	if (e.type == SDL_MOUSEBUTTONUP)
	{
		SDL_MouseButtonEvent m = e.button;
		if (m.button == SDL_BUTTON_LEFT && m.clicks == 2)
		{
			gCentrer = qadd(gCentrer, qmul(gScaler, dvec2(2.0 * m.x / gWinWidth - 1.0, 0.)));
			gCentrei = qadd(gCentrei, qmul(gScalei, dvec2(1.0 - 2.0 * m.y / gWinHeight, 0.)));
			gScaler = qmul(gScaler, dvec2(1.0 - gZoomFraction, 0.));
			gScalei = qmul(gScaler, dvec2((double)gWinHeight/gWinWidth, 0.));
/*			for (int i = 0; i < gNoVertices; i++)
			{
				gVAD[i] = -1;
			}*/
			glBindFramebuffer(GL_FRAMEBUFFER, gFramebuffer);
			glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, gTexture[gOutTexture], 0);
			glDrawBuffers(2, gDrawBuffers);
			glClearBufferiv(GL_COLOR, 1, gTexEmpty);
			checkGLError(__LINE__);

			SDL_WarpMouseInWindow(gGLWindow, gWinWidth / 2, gWinHeight / 2);
			return true;
		}
		if (m.button == SDL_BUTTON_RIGHT && m.clicks == 2)
		{
			gCentrer = qadd(gCentrer, qmul(gScaler, dvec2(2.0 * m.x / gWinWidth - 1.0, 0.)));
			gCentrei = qadd(gCentrei, qmul(gScalei, dvec2(1.0 - 2.0 * m.y / gWinHeight, 0.)));
			gScaler = qmul(gScaler, dvec2(1.0 + gZoomFraction, 0.));
			gScalei = qmul(gScaler, dvec2((double)gWinHeight/gWinWidth, 0.));
/*			for (int i = 0; i < gNoVertices; i++)
			{
				gVAD[i] = -1;
			}*/
			glBindFramebuffer(GL_FRAMEBUFFER, gFramebuffer);
			glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, gTexture[gOutTexture], 0);
			glDrawBuffers(2, gDrawBuffers);
			glClearBufferiv(GL_COLOR, 1, gTexEmpty);
			checkGLError(__LINE__);

			SDL_WarpMouseInWindow(gGLWindow, gWinWidth / 2, gWinHeight / 2);
			return true;
		}
	}
	if (e.type == SDL_MOUSEWHEEL)
	{

		if (shiftKey)
		{
			if ((e.wheel.y+e.wheel.x) * direction > 0)
			{
				if (gThreshold == gMaxThresh)
					return 0;
				gThreshold *= 1.0 + gThreshFraction;
				if (gThreshold > gMaxThresh)
					gThreshold = gMaxThresh;
//		printf("Max iterations = %i\n", gThreshold);
/*				for (int i = 0; i < gNoVertices; i++)
				{
//					if (gVAD[i] == 0)
						gVAD[i] = -1;
				}*/
				glBindFramebuffer(GL_FRAMEBUFFER, gFramebuffer);
				glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, gTexture[gOutTexture], 0);
				glDrawBuffers(2, gDrawBuffers);
				glClearBufferiv(GL_COLOR, 1, gTexEmpty);
				checkGLError(__LINE__);

				return true;
			}
			if ((e.wheel.y+e.wheel.x) *direction < 0)
			{
				if (gThreshold == gMinThresh)
					return 0;
				gThreshold /= 1.0 + gThreshFraction;
				if (gThreshold < gMinThresh)
					gThreshold = gMinThresh;
//		printf("Max iterations = %i\n", gThreshold);
/*				for (int i = 0; i < gNoVertices; i++)
				{
					if (gVAD[i] > gThreshold)
						gVAD[i] = 0;
						gVAD[i] = -1;
				}*/
				glBindFramebuffer(GL_FRAMEBUFFER, gFramebuffer);
				glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, gTexture[gOutTexture], 0);
				glDrawBuffers(2, gDrawBuffers);
				glClearBufferiv(GL_COLOR, 1, gTexEmpty);
				checkGLError(__LINE__);

				return true;
			}
			return false;
		}
		int x = 0, y = 0;
		SDL_GetMouseState(&x, &y);
		if (e.wheel.y *direction > 0)
		{
			gCentrer = qadd(gCentrer, qmul(gScaler, dvec2(2.0 * x / gWinWidth - 1.0, 0.)));
			gCentrei = qadd(gCentrei, qmul(gScalei, dvec2(1.0 - 2.0 * y / gWinHeight, 0.)));
			gScaler = qmul(gScaler, dvec2(1.0 - gZoomFraction, 0.));
			gScalei = qmul(gScaler, dvec2((double)gWinHeight/gWinWidth, 0.));

	/*		for (int i = 0; i < gNoVertices; i++)
			{
				gVAD[i] = -1;
			} */
			glBindFramebuffer(GL_FRAMEBUFFER, gFramebuffer);
			glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, gTexture[gOutTexture], 0);
			glDrawBuffers(2, gDrawBuffers);
			glClearBufferiv(GL_COLOR, 1, gTexEmpty);
			checkGLError(__LINE__);

			SDL_WarpMouseInWindow(gGLWindow, gWinWidth / 2, gWinHeight / 2);
			return true;
		}
		if (e.wheel.y * direction < 0)
		{
			gCentrer = qadd(gCentrer, qmul(gScaler, dvec2(2.0 * x / gWinWidth - 1.0, 0.)));
			gCentrei = qadd(gCentrei, qmul(gScalei, dvec2(1.0 - 2.0 * y / gWinHeight, 0.)));
			gScaler = qmul(gScaler, dvec2(1.0 + gZoomFraction, 0.));
			gScalei = qmul(gScaler, dvec2((double)gWinHeight/gWinWidth, 0.));

/*			for (int i = 0; i < gNoVertices; i++)
			{
				gVAD[i] = -1;
			}*/
			glBindFramebuffer(GL_FRAMEBUFFER, gFramebuffer);
			glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, gTexture[gOutTexture], 0);
			glDrawBuffers(2, gDrawBuffers);
			glClearBufferiv(GL_COLOR, 1, gTexEmpty);
			checkGLError(__LINE__);

			SDL_WarpMouseInWindow(gGLWindow, gWinWidth / 2, gWinHeight / 2);
			return true;
		}
	}
	if (e.type == SDL_MOUSEMOTION)
	{
		if (shiftKey)
		{
//			printf("Shift Mouse %i\n", e.motion.yrel);
			gColMapValues.y -= gHueStep * e.motion.yrel / 100.0;

			gColMapValues.x += gHueFraction * e.motion.xrel / 100.0;
			if (gColMapValues.x < 0)
				gColMapValues.x = 1.0 + gColMapValues.x;
//			SDL_WarpMouseInWindow(gGLWindow, gWinWidth / 2, gWinHeight / 2);
			return false;
		}
		if (e.motion.state & SDL_BUTTON_LMASK > 0)
		{
			gCentrer = qadd(gCentrer, -qmul(gScaler, dvec2((double)e.motion.xrel / gWinWidth)));
			gCentrei = qadd(gCentrei, qmul(gScalei, dvec2((double)e.motion.yrel / gWinHeight)));

/*			for (int i = 0; i < gNoVertices; i++)
			{
				gVAD[i] = -1;
			}*/
			glBindFramebuffer(GL_FRAMEBUFFER, gFramebuffer);
			glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, gTexture[gOutTexture], 0);
			glDrawBuffers(2, gDrawBuffers);
			glClearBufferiv(GL_COLOR, 1, gTexEmpty);
			checkGLError(__LINE__);

			return true;
		}
	}

	return false;
}

void update()
{
	//No per frame update needed
}

void close()
{
	//Deallocate program
	glDeleteProgram(gProgramID[0]);
	checkGLError(__LINE__);
	glDeleteProgram(gProgramID[1]);
	checkGLError(__LINE__);

	//Destroy window
	SDL_DestroyWindow(gGLWindow);
	gGLWindow = nullptr;

	//Quit SDL subsystems
	SDL_Quit();
}

int main(int argc, char *args[])
{
	//Start up SDL and create window
	if (!init())
	{
		printf("Failed to initialize!\n");
		return 1;
	}
	else
	{
		//Main loop flag
		bool quit = false;

		int res = gMinRes, prevRes = 1;
		int noIndices = 0;

		//Event handler
		SDL_Event e;

		bool redraw = true;

		//		printf("Starting loop\n");
		//While application is running
		while (!quit)
		{
			if (res != prevRes)
			{
				int pointWidth = gWinWidth * POINT_SCALE / res;
				int pointHeight = gWinHeight * POINT_SCALE / res;
				noIndices = pointWidth * pointHeight;
				if (gIBD != nullptr)
				{
					//					printf("About to delete gIBD\n");
					delete[] gIBD;
				}
				//				printf("About to create gIBD\n");
				gIBD = new GLuint[noIndices];
				int i = 0;
				int ystep = gWinWidth * POINT_SCALE;
				//				printf ("About to set indices\n");
				for (int v = 0; v < pointHeight; v++)
				{
					int y = (v+1)/2*((v%2)*2-1)+(pointHeight-1)/2;
					int basey = y * res * ystep;
					for (int w = 0; w < pointWidth; w++)
					{
						int x = (w+1)/2*((w%2)*2-1)+(pointWidth-1)/2;
						int base = x * res + basey;
						if (i < noIndices)
						{
							gIBD[i++] = base;
						}
						else
						{
							printf("Indices %i, i %i\n", noIndices, i);
							printf("Overflow gIBD. Window (%i,%i) loop (%i,%i) res %i\n", gWinWidth, gWinHeight, x, y, res);
						}
/*						int parentX = (x / 2) * 2;
						int parentY = (y / 2) * 2;
						int parentB = parentX * res + (parentY * res * ystep);
						int parentI = gVAD[parentB];
						if (parentI > -1 && parentB != base)
						{
							bool same = true;
							for (int pY = -1; pY < 2; pY++)
							{
								for (int pX = -1; pX < 2; pX++)
								{
									int peerB = parentX + pX * res * 2 + (parentY + pY * res * 2) * ystep;
									if (peerB >= 0 && peerB < gNoVertices)
									{
										if (gVAD[peerB] != parentI)
										{
											same = false;
										}
									}
								}
							}
							if (same)
								if (base < gNoVertices)
								{
									gVAD[base] = parentI;
								}
								else
								{
									printf("Vertices %i, base %i\n", gNoVertices, base);
								}
							}
						} */
					}
				}
				//				printf("Have set indices\n");
				glBindVertexArray(gTextureVA);
				checkGLError(__LINE__);

				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gIBO);
				checkGLError(__LINE__);

				glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * noIndices, gIBD, GL_STATIC_DRAW);
				checkGLError(__LINE__);
			}

			int indstep = noIndices;
			GLint total_iter = noIndices * gThreshold * (1 + (gHighCapFactor - 1)*gProgram);
			while (total_iter > gFrameCap && indstep > 1 && (prevRes!=1 || res != 1))
			{
				total_iter /= 2;
				indstep /= 2;
			}
			bool haltLoop = false;
			for (int i = 0; i < noIndices && !quit && !haltLoop; i += indstep)
			{
				if (prevRes != 1 || res != 1) {
					glBindFramebuffer(GL_FRAMEBUFFER, gFramebuffer);
					glFramebufferTexture(GL_READ_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, gTexture[gOutTexture], 0);
					glFramebufferTexture(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, gTexture[1-gOutTexture], 0);
					glReadBuffer(GL_COLOR_ATTACHMENT1);
					glDrawBuffer(GL_COLOR_ATTACHMENT0);
					glBlitFramebuffer(0, 0, gWinWidth, gWinHeight, 0, 0, gWinWidth, gWinHeight, GL_COLOR_BUFFER_BIT, GL_NEAREST);
					gOutTexture = 1 - gOutTexture;

					printf("\rResolution %i Threshold %i %i%%",res, gThreshold, (int)((100.0*i/noIndices)));
					fflush(stdout);
					glBindVertexArray(gTextureVA);
					checkGLError(__LINE__);

					glUseProgram(gProgramID[gProgram]);
					checkGLError(__LINE__);

					// 1st attribute buffer : vertices
					glEnableVertexAttribArray(0);
					checkGLError(__LINE__);

					glBindBuffer(GL_ARRAY_BUFFER, gVBO);
					checkGLError(__LINE__);

					glVertexAttribIPointer(
						0,		  // attribute 0. No particular reason for 0, but must match the layout in the shader.
						2,		  // size
						GL_INT, // type
//						GL_FALSE, // normalized?
						0,		  // stride
						(void *)0 // array buffer offset
					);
					checkGLError(__LINE__);

/*					glEnableVertexAttribArray(1);
					checkGLError(__LINE__);

					glBindBuffer(GL_ARRAY_BUFFER, gVAB);
					checkGLError(__LINE__);

					glBufferData(GL_ARRAY_BUFFER, sizeof(GLint) * gNoVertices, gVAD, GL_STATIC_DRAW);
					checkGLError(__LINE__);

					glVertexAttribIPointer(1, 1, GL_INT, 0, (void *)0);
					checkGLError(__LINE__);

					glBindBuffer(GL_ARRAY_BUFFER, gTFB);
					checkGLError(__LINE__);

					glBufferData(GL_ARRAY_BUFFER, sizeof(GLint) * gNoVertices, nullptr, GL_STATIC_READ);
					checkGLError(__LINE__);

					glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, gTFB);
					checkGLError(__LINE__); */

					glActiveTexture(GL_TEXTURE0);
					checkGLError(__LINE__);

					glBindTexture(GL_TEXTURE_2D, gTexture[1-gOutTexture]);
					checkGLError(__LINE__);
					
					glUniform1i(gInTexLocation, 0);
					checkGLError(__LINE__);

					glUniform4d(gCentreLocation, gCentrer.x, gCentrer.y, gCentrei.x, gCentrei.y);
					checkGLError(__LINE__);

					glUniform4d(gScaleLocation, gScaler.x, gScaler.y, gScalei.x, gScalei.y);
					checkGLError(__LINE__);

					glUniform4i(gParamsLocation, gThreshold,gWinWidth,gWinHeight,res);
					checkGLError(__LINE__);

	//				glUniform2f(gColMapLocation, gColMapValues.x, gColMapValues.y);
	//				checkGLError(__LINE__);

					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gIBO);
					checkGLError(__LINE__);

/*					GLuint query;
					glGenQueries(1, &query);
					checkGLError(__LINE__);

					glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query);
					checkGLError(__LINE__);

					glBeginTransformFeedback(GL_POINTS);
					checkGLError(__LINE__); */

//					glPointSize(res);
//					checkGLError(__LINE__);

					int noElements = indstep;
					if (i+noElements >= noIndices) noElements = noIndices - i;

					glBindFramebuffer(GL_FRAMEBUFFER, gFramebuffer);
					checkGLError(__LINE__);

					glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, gTexture[gOutTexture], 0);
					checkGLError(__LINE__);

					glDrawBuffers(2, gDrawBuffers);
					checkGLError(__LINE__);

					if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
						printf("Framebuffer not complete at line %i\n",__LINE__);

					glViewport(0,0,gWinWidth,gWinHeight);

					glDrawElements(GL_POINTS, noElements, GL_UNSIGNED_INT, (void *)(i * sizeof(GLuint)));
					checkGLError(__LINE__);

/*					glEndTransformFeedback();
					checkGLError(__LINE__);

					glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);
					checkGLError(__LINE__);

					GLuint primitives;
					glGetQueryObjectuiv(query, GL_QUERY_RESULT, &primitives);
					checkGLError(__LINE__); */

					//				printf("i %i, Primitives %i, resolution %i\n", i, primitives, res);

					glDisableVertexAttribArray(0);
					checkGLError(__LINE__);

/*					glDisableVertexAttribArray(1);
					checkGLError(__LINE__);

					glGetBufferSubData(GL_TRANSFORM_FEEDBACK_BUFFER, 0, sizeof(GLint) * gNoVertices, gTFD);
					checkGLError(__LINE__);
					if (primitives + i > noIndices)
					{
						printf("Primitives %i, i %i, indices %i\n", primitives, i, noIndices);
					}
					else
					{
						for (int p = 0; p < primitives; p++)
						{
							if (gIBD[i + p] < gNoVertices)
							{
								if (gVAD[gIBD[i + p]] < 0)
									gVAD[gIBD[i + p]] = gTFD[p];
							}
							else
							{
								printf("i %i, p %i, gIBD %i, vertices %i\n", i, p, gIBD[i + p], gNoVertices);
							}
						}
					} */
				}

				glBindVertexArray(gScreenVA);
				checkGLError(__LINE__);

				glUseProgram(gScreenPID);
				checkGLError(__LINE__);

				glEnableVertexAttribArray(0);
				checkGLError(__LINE__);

				glBindBuffer(GL_ARRAY_BUFFER, gScreenBO);
				checkGLError(__LINE__);

				glVertexAttribPointer(
					0,		  // attribute 0. No particular reason for 0, but must match the layout in the shader.
					2,		  // size
					GL_FLOAT, // type
					GL_FALSE, // normalized?
					0,		  // stride
					(void *)0 // array buffer offset
				);
				checkGLError(__LINE__);

				glBindFramebuffer(GL_FRAMEBUFFER, 0);
				checkGLError(__LINE__);

				glViewport(0,0,gWinWidth, gWinHeight);
				checkGLError(__LINE__);

				glActiveTexture(GL_TEXTURE0);
				checkGLError(__LINE__);

				glBindTexture(GL_TEXTURE_2D, gTexture[gOutTexture]);
				checkGLError(__LINE__);
				
				glUniform1i(gTextureLocation, 0);
				checkGLError(__LINE__);

				glUniform4i(gScrParamLocation, gThreshold, res, gWinWidth, gWinHeight);
				checkGLError(__LINE__);

				glUniform2f(gColMapLocation, gColMapValues.x, gColMapValues.y);
				checkGLError(__LINE__);

				// Draw the triangle !
				glDrawArrays(GL_TRIANGLES, 0, 2*3); // 12*3 indices starting at 0 -> 12 triangles -> 6 squares
				checkGLError(__LINE__);

				glDisableVertexAttribArray(0);
				checkGLError(__LINE__);
				//Update screen
				SDL_GL_SwapWindow(gGLWindow);
				glFlush();

/*				glReadBuffer(GL_FRONT);
				checkGLError(__LINE__);

				glDrawBuffer(GL_BACK);
				checkGLError(__LINE__);

				glBlitFramebuffer(0, 0, gWinWidth, gWinHeight,
								  0, 0, gWinWidth, gWinHeight, GL_COLOR_BUFFER_BIT, GL_NEAREST);
				checkGLError(__LINE__);

				//Update screen
				SDL_GL_SwapWindow(gGLWindow);

				glReadBuffer(GL_BACK);
				checkGLError(__LINE__);

				glDrawBuffer(GL_FRONT);
				checkGLError(__LINE__); */

				redraw = false;
				while (SDL_PollEvent(&e) != 0 && !quit)
				{
					//User requests quit
					if (e.type == SDL_QUIT)
					{
						quit = true;
					}
					else if (e.type == SDL_KEYDOWN)
					{
						if (e.key.keysym.sym == SDLK_q)
						{
							quit = true;
						}
					}
					else if (e.type == SDL_KEYUP)
					{
						int x = 0, y = 0;
						SDL_GetMouseState(&x, &y);
						redraw = redraw || handleKeys(e.key.keysym, x, y);
					}
					else if (e.type == SDL_WINDOWEVENT)
					{
						redraw = redraw || handleWindow(e.window);
					}
					else if (e.type == SDL_MOUSEMOTION || e.type == SDL_MOUSEWHEEL || e.type == SDL_MOUSEWHEEL || e.type == SDL_MOUSEBUTTONUP || e.type == SDL_MOUSEBUTTONDOWN)
						redraw = redraw || handleMouse(e);
				}
				if (redraw) haltLoop = true;
			}
			if (haltLoop) {
				printf("...interrupted\n");
				res = gMinRes;
			} else {
				if (prevRes != res) {
					if (res == 1) printf("\rFinished. centre (%.10e,%.10e) scale (%.10e,%.10e)\n", gCentrer.x, gCentrei.x, gScaler.x, gScalei.x);
					else printf("\rResolution %i Threshold %i 100%%\n", res, gThreshold);
				}
				prevRes = res;
				if (res > 1) res /= 2;
			}
		}

		//Free resources and close SDL
		close();

		return 0;
	}
}

GLuint LoadShaders(bool feedback, const char *vertex_file_path, const char *fragment_file_path)
{

	// Create the shaders
	GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
	GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

	// Read the Vertex Shader code from the file
	std::string VertexShaderCode;
	std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
	if (VertexShaderStream.is_open())
	{
		std::stringstream sstr;
		sstr << VertexShaderStream.rdbuf();
		VertexShaderCode = sstr.str();
		VertexShaderStream.close();
	}
	else
	{
		printf("Impossible to open %s. Are you in the right directory ? Don't forget to read the FAQ !\n", vertex_file_path);
		getchar();
		return 0;
	}

	// Read the Fragment Shader code from the file
	std::string FragmentShaderCode;
	std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
	if (FragmentShaderStream.is_open())
	{
		std::stringstream sstr;
		sstr << FragmentShaderStream.rdbuf();
		FragmentShaderCode = sstr.str();
		FragmentShaderStream.close();
	}

	GLint Result = GL_FALSE;
	int InfoLogLength;

	// Compile Vertex Shader
	printf("Compiling shader : %s\n", vertex_file_path);
	char const *VertexSourcePointer = VertexShaderCode.c_str();
	glShaderSource(VertexShaderID, 1, &VertexSourcePointer, NULL);
	glCompileShader(VertexShaderID);

	// Check Vertex Shader
	glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0)
	{
		std::vector<char> VertexShaderErrorMessage(InfoLogLength + 1);
		glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
		printf("%s\n", &VertexShaderErrorMessage[0]);
	}

	// Compile Fragment Shader
	printf("Compiling shader : %s\n", fragment_file_path);
	char const *FragmentSourcePointer = FragmentShaderCode.c_str();
	glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer, NULL);
	glCompileShader(FragmentShaderID);

	// Check Fragment Shader
	glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
	glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0)
	{
		std::vector<char> FragmentShaderErrorMessage(InfoLogLength + 1);
		glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
		printf("%s\n", &FragmentShaderErrorMessage[0]);
	}

	// Link the program
	printf("Linking program\n");
	GLuint ProgramID = glCreateProgram();
	glAttachShader(ProgramID, VertexShaderID);
	glAttachShader(ProgramID, FragmentShaderID);

	/* if (feedback) {
	const GLchar *feedbackVaryings[] = {"outValue"};
	glTransformFeedbackVaryings(ProgramID, 1, feedbackVaryings, GL_INTERLEAVED_ATTRIBS);
	} */

	glLinkProgram(ProgramID);

	// Check the program
	glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
	glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
	if (InfoLogLength > 0)
	{
		std::vector<char> ProgramErrorMessage(InfoLogLength + 1);
		glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
		printf("%s\n", &ProgramErrorMessage[0]);
	}

	glDetachShader(ProgramID, VertexShaderID);
	glDetachShader(ProgramID, FragmentShaderID);

	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	return ProgramID;
}
#version 410 core
#pragma optionNV(fastmath off)
#pragma optionNV(fastprecision off)

layout(location=0) in ivec2 UV;

/* layout(location=1) in int inValue;

out int outValue; */

flat out int itCount;

uniform dvec4 centre;
uniform dvec4 scale;

uniform ivec4 params;
int maxI = params.x;
int width = params.y;
int height = params.z;
int res = params.w;

//uniform vec2 colmap;

uniform isampler2D inTex;

dvec2 quadadd (dvec2 dsa, dvec2 dsb)
{
dvec2 dsc;
double t1, t2, e;

t1 = dsa.x + dsb.x;
e = t1 - dsa.x;
t2 = ((dsb.x - e) + (dsa.x - (t1 - e))) + dsa.y + dsb.y;

dsc.x = t1 + t2;
dsc.y = t2 - (dsc.x - t1);
return dsc;
}

dvec2 quadmul (dvec2 dsa, dvec2 dsb)
{
dvec2 dsc;
double c11, c21, c2, e, t1, t2;
double a1, a2, b1, b2, cona, conb, split = 536870913.;

cona = dsa.x * split;
conb = dsb.x * split;
a1 = cona - (cona - dsa.x);
b1 = conb - (conb - dsb.x);
a2 = dsa.x - a1;
b2 = dsb.x - b1;

c11 = dsa.x * dsb.x;
c21 = a2 * b2 + (a2 * b1 + (a1 * b2 + (a1 * b1 - c11)));

c2 = dsa.x * dsb.y + dsa.y * dsb.x;

t1 = c11 + c2;
e = t1 - c11;
t2 = dsa.y * dsb.y + ((c2 - e) + (c11 - (t1 - e))) + c21;

dsc.x = t1 + t2;
dsc.y = t2 - (dsc.x - t1);

return dsc;
}

/* vec3 hsv2rgb(vec3 c)
{
    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
}

vec3 iter2col(int iter) {
	float r = log(float(iter))/log(float(maxI));
	return hsv2rgb(vec3(colmap.x - r*colmap.y,1.0,1.0));
} */

						
void main(){
	gl_Position.xy = vec2(2.0*float(UV.x)/(width-1)-1.0, 2.0*float(UV.y)/(height-1)-1.0);
	gl_Position.z = 0.0;
	gl_Position.w = 1.0;

//	vec2 tUV = vec2(UV.x/2.0+0.5, UV.y/2.0-0.5);

    int iv = texelFetch( inTex, UV, 0).r;


//	int iv = inValue;


	if (iv < 0) {
		ivec2 parent = ivec2((UV.x / 2/ res) * 2 * res,(UV.y / 2/ res) * 2 * res);
		int parentI = texelFetch( inTex, parent, 0).r;
		bool same = false;
		if (parentI > -1 && parent != UV)
		{
			same = true;
			for (int pY = -1; pY < 2; pY++)
			{
				for (int pX = -1; pX < 2; pX++)
				{
					ivec2 peer = ivec2(parent.x + 2 * res * pX, parent.y + 2 * res * pY);
					if (peer.x >= 0 && peer.y >= 0 && peer.x < width && peer.y < height)
					{
						if (texelFetch(inTex, peer, 0).r != parentI)
						{
							same = false;
						}
					}
				}
			}
		}
		if (same) itCount = parentI;
		else
		{
			dvec2 qU = dvec2(gl_Position.x, 0.0);
			dvec2 qV = dvec2(gl_Position.y, 0.0);

			dvec2 cr = quadadd(centre.xy,quadmul(qU,scale.xy));
			dvec2 ci = quadadd(centre.zw,quadmul(qV,scale.zw));

			dvec2 zr = cr;
			dvec2 zi = ci;
			dvec2 q2 = dvec2(2.0, 0.0);

			dvec2 mag, zr2, zi2;

	//		outValue = 0;
			itCount = 0;
			for (int i=0;i<maxI;i++) {
				zr2 = quadmul(zr,zr);
				zi2 = quadmul(zi,zi);
				mag = quadadd(zr2,zi2);
				if (mag.x > 4.0) {
	//				outValue = i;
					itCount = i;	
					break;
				}
				zi = quadadd(quadmul(quadmul(zi, zr), q2), ci);
				zr = quadadd(quadadd(zr2, -zi2), cr);
			}
		}
	} else {
//		outValue = iv;
		itCount = iv;
	}

}